#!/bin/bash

set -eu

TYPE="${1:-memory}"
declare -a CONTAINER_LIMITS
declare perf_pid

case "$TYPE" in
    "cpu")
        echo "Start test: limit CPU"
	CONTAINER_LIMITS=(--cpu-shares 512 --cpuset-cpus 0)
        echo "Summary: add limit with ${CONTAINER_LIMITS[*]}"
        ;;
    "memory")
        echo "Start test: limit memory"
	CONTAINER_LIMITS=(--memory 512m)
        echo "Summary: add limit with ${CONTAINER_LIMITS[*]}"
        ;;
    *)
        echo "Start test: Basic"
        echo "Summary: default container options"
	;;
esac

echo "Monitor the Orderly container health"
./performance.sh &
perf_pid=$!

echo "Create Orderly container"
podman run -d --rm \
       --name orderly stress-container sh -c "stress-ng --tmpfs 50 --timeout 50"

echo "Create Confusion container"
podman run -d --rm "${CONTAINER_LIMITS[@]}" \
       --name confusion stress-container sh -c "stress-ng --matrix 0 -t 1m"

# Wait until both containers have finished
podman wait orderly confusion

# Kill the monitor script
pkill -P "$perf_pid"

echo "End test"
